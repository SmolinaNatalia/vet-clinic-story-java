public class Animal {
    private String name;
    private String disease;
    public Animal(String name, String disease){
        this.name = name;
        this.disease = disease;
    }

    public String getName(){
        return name;
    }

    public String getDisease(){
        return disease;
    }

}