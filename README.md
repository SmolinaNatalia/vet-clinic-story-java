# <story name>
# Vet Clinic Story

**To read**: [<link to story opened in Refactories Story UI, just link from browser is good enough>]

**Estimated reading time**: 15 min

## Story Outline
The purpose of this coding story is to show how enum works in java and why it was added to java. Moreover, this story shows some basic methods of enum.

## Story Organization
**Story Branch**: master
> `git checkout master`


Tags: #enum